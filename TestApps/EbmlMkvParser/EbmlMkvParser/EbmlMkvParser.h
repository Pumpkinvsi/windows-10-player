#pragma once

#include <iostream>
#include <fstream>
#include <tchar.h>
#include <string>
#include <iomanip>
#include "EbmlTag.h"

bool checkEBMLHeader(); 
int bigEndianToLittleEndian(int d);