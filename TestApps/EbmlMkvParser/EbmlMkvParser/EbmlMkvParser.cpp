#include "stdafx.h"
#include "EbmlMkvParser.h"

using namespace std;

int main()
{
	string *filePath = new string();
	cout << "Type path to file: ";
	getline(cin, *filePath);
	cin.clear();
	if (filePath->length() == 0)
	{
		filePath->append("C:\\Users\\avolkov\\Documents\\Projects\\Win10Player\\TestApps\\test mkv\\test1.mkv");
		cout << "C:\\Users\\avolkov\\Documents\\Projects\\Win10Player\\TestApps\\test mkv\\test1.mkv \n";
	}
	std::ifstream fileStream;

	fileStream.open(filePath->c_str(), ios::binary | ios::in);
	EbmlTag *test = new EbmlTag(&fileStream);
	//checkEBMLHeader();

	cin.ignore();
	cin.get();
    return 0;
}

bool checkEBMLHeader(std::ifstream *fileStream)
{
	if (!fileStream)
	{
		cout << "File not found";
		return false;
	}

	/*char ebmlCheckHeader[4];
	for (int i = 0;i < 4;i++)
	{
		FileStream.get(ebmlCheckHeader[i]);
	}*/

	int ebmlCheckHeader;
	fileStream->read((char*)&ebmlCheckHeader, 4);
	ebmlCheckHeader = bigEndianToLittleEndian(ebmlCheckHeader);
	if (ebmlCheckHeader != 0x1A45DFA3)
	{
		cout << hex << ebmlCheckHeader << endl << "Is not begining of ebml file";
		return false;
	}

	cout << "Got valid EBML file";
	return true;
}

int bigEndianToLittleEndian(int d)
{
	int a;
	unsigned char *dst = (unsigned char *)&a;
	unsigned char *src = (unsigned char *)&d;

	dst[0] = src[3];
	dst[1] = src[2];
	dst[2] = src[1];
	dst[3] = src[0];

	return a;
}



