#include <fstream>

class EbmlTag
{
public:
	EbmlTag(std::ifstream *source);
	~EbmlTag();

private:
	int length = 0;
	int id = 0;

	int GetValueLength(_int8 word);
	void GetId(std::ifstream *source);
	void GetLength(std::ifstream *source);
};
