#include "stdafx.h"
#include "EbmlTag.h"

EbmlTag::EbmlTag(std::ifstream *source)
{
	GetId(source);
	GetLength(source);
}

EbmlTag::~EbmlTag()
{
}

int EbmlTag::GetValueLength(_int8 word)
{
	if ((unsigned char)word > 0x80)
	{
		return 1;
	}
	else if ((unsigned char)word > 0x40)
	{
		return 2;
	}
	else if ((unsigned char)word > 0x20)
	{
		return 3;
	}
	else if ((unsigned char)word > 0x10)
	{
		return 4;
	}
	else
	{
		// exception
	}
}

void EbmlTag::GetId(std::ifstream * source)
{
	int length = 0;
	this->id = 0;
	_int8 firstWord = 0;
	source->read(&firstWord, 1);
	length = GetValueLength(firstWord);
	_int8 *id = (char*)&(this->id);
	id[length - 1] = firstWord;
	for (int i = length - 2; i >= 0; i--)
	{
		source->read(&id[i], 1);
	}
}

void EbmlTag::GetLength(std::ifstream * source)
{
	int len = 0;
	this->length = 0;
	_int8 firstWord = 0;
	source->read(&firstWord, 1);
	len = GetValueLength(firstWord);
	_int8 *_length = (char*)&(this->length);
	//Debug: _int8 clearLength = firstWord ^ (1 << 8 - len);
	_length[len - 1] = firstWord ^ (1 << 8 - len);
	for (int i = len - 2; i >= 0; i--)
	{
		source->read(&_length[i], 1);
	}
}
