﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace UniversalHelloWorld.Model
{
    class HelloModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private string _firstName;
        public string FirstName { get { return _firstName; }
            set { _firstName = value; NotifyPropertyChanged("FirstName"); NotifyPropertyChanged("FullName"); }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; NotifyPropertyChanged("LastName"); NotifyPropertyChanged("FullName"); }
        }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}
