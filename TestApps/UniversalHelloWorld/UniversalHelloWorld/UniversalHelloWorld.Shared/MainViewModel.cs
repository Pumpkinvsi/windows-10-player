﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using UniversalHelloWorld.Model;

namespace UniversalHelloWorld
{
    class MainViewModel
    {
        public HelloModel Test { get; set; }

        public string tst = "Hello";

        public MainViewModel()
        {
            Test = new HelloModel();
            Test.FirstName = "Hello";
            Test.LastName = "World";
        }

        public string test2 { get; set; }
    }
}
